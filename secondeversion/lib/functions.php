<?php

function generateString($length = 32) {
	$result = "";
	$pattern = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
	$pattern_length = strlen($pattern);
	
	for ($i=0; $i<$length; $i++) {
		$result .= $pattern[rand(0, ($pattern_length-1))];
	}
	
	return $result;
}

function passwordCNIL($password) {
	$result = TRUE;
	
	//Au moins 8 caractères
	if (strlen($password)<8) {
		$result = FALSE;
	}
	
	//Au moins 1 chiffre
	if (!preg_match("/[0-9]/",$password)) {
		$result = FALSE;
	}
	
	//Au moins une lettre majuscule
	if (!preg_match("/[A-Z]/",$password)) {
		$result = FALSE;
	}
	
	//Au moins un caractère spécial !&~"#'{([-|`_\^@°)]=+}§,?;.:/
	if (!preg_match("/[^a-zA-Z0-9]+/",$password)) {
		$result = FALSE;
	}
	
	return $result;
}

function checkDifferentPassword($new_password, $old_passwords) {
	$result = TRUE;
	
	//Désérialiser = chaine -> tableau
	$array_old = unserialize($old_passwords);
	
	foreach($array_old as $old_password) {
		if (password_verify($new_password, $old_password)) {
			$result = FALSE;
			break;
		}
	}
	
	return $result;
}








