<?php
if (isset($_POST['subscribe'])) {
	//Déclare les vars
	$form_login = $_POST['login'];
	$form_password = $_POST['password'];
	$form_password_repeat = $_POST['password_repeat'];
	$form_valid = TRUE;
	$form_error_msg = "";
	
	//Assainir: XSS, injection SQL 
	$form_login = trim(strip_tags($form_login));
	
	//Encrypter le mot de passe
	$form_password = password_hash($form_password, PASSWORD_DEFAULT);
	
	/* VERIFICATION */
	
	//Est-ce que les champs sont saisis ?
	if (empty($form_login)) {
		$form_valid = FALSE;
		$form_error_msg = "Login empty";
	}
	if (empty($form_password)) {
		$form_valid = FALSE;
		$form_error_msg = "Password empty";
	}
	
	//Comparer les mot de passe
	if (!password_verify($form_password_repeat, $form_password)) {
		//Mot de passe pas similaire
		$form_valid = FALSE;
		$form_error_msg = "Passwords do not match";
	}
	
	//Est-ce que c'est un mail ?
	if (!filter_var($form_login, FILTER_VALIDATE_EMAIL)) {
		$form_valid = FALSE;
		$form_error_msg = "Not an email";
	}
	
	if ($form_valid) {
		/* VERIFICATION AVEC BDD */
		
		//Est-ce que le mail existe ou pas dans la BDD ?
		$req = "SELECT id
		FROM user
		WHERE mail='".$form_login."'";
		$res = $GLOBALS['db']->query($req);
		if ($res->num_rows) {
			$form_valid = FALSE;
			$form_error_msg = "Mail account already exists";
		}
		
		/* FIN VERIFICATIONS, AJOUT */
		if ($form_valid) {
			$activation_string = generateString(32);
			
			$req = "INSERT INTO user
			(mail, password, activation_string) 
			VALUES('".$form_login."', '".$form_password."', '".$activation_string."')";
			
			$GLOBALS['db']->query($req);
			if (!empty($GLOBALS['db']->error)) {
				//Erreur
				$form_error_msg = $GLOBALS['db']->error;
			} else {
				$form_error_msg = "Account created";
				
				//Envoyer un mail avec la string activation
				$activation_url = "toto.com/activate.php
				?id=".$GLOBALS['db']->insert_id
				."&string=".$activation_string;
			}
		}
	}
}
?>