<?php
    session_start();

    require_once('./bdd_connect.php');

    //On supprimes l'utilisateur
    $bdd->query('DELETE FROM `user` WHERE `user`.`user` = "'.$_SESSION["user"].'"');

    //On supprime toutes les sessions et cookies
    if (ini_get("session.use_cookies")) {
        $params = session_get_cookie_params();
        setcookie(session_name(), '', time() - 42000,
            $params["path"], $params["domain"],
            $params["secure"], $params["httponly"]
        );
    }
    session_destroy();

    //On redirige vers la page de connexion
    header('Location: ../src/index.php');


?>