<?php
session_start();
$host = "127.0.0.1";
$login = "root";
$password = "";
$db = "ecf_chat";

$GLOBALS['db'] = new mysqli($host, $login, $password, $db);
require_once('../lib/functions.php');

//Connexion par cookie
if (isset($_COOKIE['connexion'])) {
	$cookie_string = addslashes($_COOKIE['connexion']);
	
	$req = "SELECT id FROM user WHERE cookie_string='{$cookie_string}'";
	$res = $GLOBALS['db']->query($req);
	if ($res->num_rows) {
		$row = $res->fetch_assoc();

		//Session
		$_SESSION['id'] = $row['id'];
		$_SESSION['user'] = $row['user'];
		//Envoi du cookie au client
        setcookie("connexion", $_COOKIE['connexion'], time()+60*60*24*14);
	}
}

//Connexion par formulaire
if (isset($_POST['login_button'])) {
	//Déclare les vars
	$form_login = $_POST['login'];
	$form_password = $_POST['password'];

	$form_error_msg = "";
	
	//Assainir: XSS, injection SQL 
	$form_login = addslashes(trim(strip_tags($form_login)));
	
	/* VERIFICATION AVEC BDD */
	
	//Est-ce que le mail existe ou pas dans la BDD ?
	$req = "SELECT id, user, password
	FROM user
	WHERE mail='".$form_login."'";
	$res = $GLOBALS['db']->query($req);
	if ($res->num_rows) {
		//Le mail existe
		$row = $res->fetch_assoc();
		
		//Tester le mot de passe
		if (password_verify($form_password, $row['password'])) {
			//Le mot de passe est ok
			//Connexion OK !
			
			//Session
			$_SESSION['id'] = $row['id'];
			$_SESSION['user'] = $row['user'];
			
			//Cookie
			$cookie_value = generateString(32);
			
			//Mise à jour BDD
			$req_cookie = "UPDATE user
			SET cookie_string='{$cookie_value}'
			WHERE id=".$row['id'];
			$GLOBALS['db']->query($req_cookie);
			
			//Envoi du cookie au client
            setcookie("connexion", $cookie_value, time()+60*60*24*14);
            header('Location: ../src/dashboard.php');
		} else {
			echo  "Le login ou le mot de passe sont incorrects ou inexistants";
		}
	} else {
		//Le mail n'existe pas
		echo "Le login ou le mot de passe sont incorrects ou inexistants";
	}
}


?>