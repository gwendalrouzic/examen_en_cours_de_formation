<?php 
    session_start();
    //On supprimes toutes sessions et cookies
    if (ini_get("session.use_cookies")) {
        $params = session_get_cookie_params();
        setcookie(session_name(), '', time() - 42000,
            $params["path"], $params["domain"],
            $params["secure"], $params["httponly"]
        );
    }
    session_destroy();

    //On redirige vers connexion
    header('Location: ../src/index.php');
?>