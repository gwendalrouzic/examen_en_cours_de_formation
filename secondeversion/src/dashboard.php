<?php session_start(); ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="../styles/dashboard.css">
    <title>Dashboard</title>
</head>
<body>
    <h1>Connecté</h1>
    <div class="comment_window">
        <?php require_once('../components/chat_request.php');?>
    </div>

    <form action="../components/chat_post.php" method="post">
        <input type="text" placeholder="pseudo" name="name" id="name" value="<?php echo $_SESSION['user'] ?>">
        <textarea name="comment" id="comment" placeholder="message"></textarea>
        <input type="submit" value="envoyer" id="envoi">
    </form>

    <a href="../components/disconnect.php">déconnexion</a>
    <a href="./parametre.php">paramètres</a>
   
</body>
</html>